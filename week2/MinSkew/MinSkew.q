/MinSkew.q
/1.3 Peculiar Statistics of the Forward and Reverse Half-Strands

/q code
/Set l as the input genome and l2 as string of zeros of same length.
l2:(count l)#0

/Use group function to group the string into unique nucleotides.
d:group l

/At indices of "C", set l2 as -1.
l2[d["C"]]:-1

/At indices of "G", set l2 as 1.
l2[d["G"]]:1

/add 0 to beginning of string and assign to l3.
l3:0,l2

/Take cumulative sums of the items of l3 using sums function.
l4:sums l3

/Find the index where the cumulative sum was minimum. This gives us the minimum skew.
minSkewIndices:t min key t:group l4 


/sample input
l:"CATGGGCATCGGCCATACGCC"

/file input
filePath:`$":week2/MinSkew/dataset_7_10_2.txt"
data:read0 filePath
l:data[0]

