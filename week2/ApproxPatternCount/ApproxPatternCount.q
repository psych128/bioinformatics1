/ApproxPatternCount.q
/1.4 Some Hidden Messages are More Elusive than Others


/qcode
/Assign Text to l1 and Pattern to l2
/Start by taking all possible sublists of l1 of length k.
/Then compare them to given Pattern.
/Finally, sum all the mismatches and look for those sublists that have less than k mismatches. Finally, use the count function to get the total number of instances

count where k>=sum each not l2=/:sublist[;l1] each neg[count[l2]-1]_flip (til count l1;count l2)

/sample input
l1:"AACAAGCTGATAAACATTTAAAGAG";
l2:"AAAAA";
k:2;


/file input
filePath:`$":week2/ApproxPatternCount/dataset_9_6_3.txt"
data:read0 filePath
l1:data[1]
l2:data[0]
k:value first data[2]
