/HammingDist.q
/1.4 Some Hidden Messages are More Elusive than Others

/qcode
/Use equal operator to item-wise compare the 2 strings.
/Use not operator to inverse the result and use sum to find total number of mismatches.
sum not l1=l2

/sample input
l1:"GGGCCGTTGGT"
l2:"GGACCGTTGAC"

/file input
filePath:`$":week2/HammingDist/dataset_9_3_2.txt"
data:read0 filePath
l1:data[0]
l2:data[1]

