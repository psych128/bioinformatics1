/ReverseComplement.q
/Chapter 1.3 Some Hidden Messages are More Surprising than Others


/q code
reverseComplement:{[dnaStrandSym]
    dnaDict:(`A`T`G`C)!(`T`A`C`G);
    :dnaStrandComp:reverse dnaDict dnaStrandSym
    };

/testing with sample input
dnaStrandSym:`A`A`A`A`C`C`C`G`G`T
/sample output - ACCGGGTTTT


/file input
filePath:`$":week1/ReverseComplement/dataset_3_2.txt"
dnaStrand:read0 filePath
dnaStrandSym:`$/: first dnaStrand
flip string reverseComplement dnaStrandSym

/saving output
(`$":ReverseComplement/dataset_3_2_ans.txt") 0: flip string reverseComplement dnaStrandSym

