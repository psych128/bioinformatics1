/ClumpFinding.q
/Chapter 1.4 An Explosion of Hidden Messages


/q code
/divide given genome into all possible sections of length L
l2:l where b[1]=count each l:sublist[;a] each flip (til count a;b[1])

/divide each section of length L into subsections of length k
/create frequency map of each subsection
f:{[a;b] count each group sublist[;a] each flip (til count a;b)}
fm:f[;b[0]] each l2

/identify all subsections that occur t times in sections of length L
fcount:{[freqMap;cnt] (key freqMap) where (value freqMap) = cnt}
distinct raze fcount[;b[2]] each fm


/sample input
a:"CGGACTCGACAGATGTGAAGAACGACAATGTGAAGACTCGACACGACAGAGTGAAGAGAAGAGGAAACATTGTAA"
b:(5;50;4)

/file input
filePath:`$":week1/ClumpFinding/dataset_4_5_3.txt"
data:read0 filePath
a:data[0]
b:value data[1]



/ecoli
filePath:`$":week1/ClumpFinding/E_coli.txt"
data:read0 filePath
genome:data[0]
inputs:(9;500;3)

slParams:flip (til ((count genome)-(inputs[1]-1));inputs[1])
count slParams
.gl.counter:0
f:{[sl;k;L;t]
    l:sublist[sl;genome];
    fm:count each group sublist[;l] each flip (til count l;k);    
    :(key fm) where (value fm) = t};

res:raze f[;inputs[0];inputs[1];inputs[2]] peach slParams
