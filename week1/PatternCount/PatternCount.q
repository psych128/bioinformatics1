/PatternCount.q
/Chapter 1.2 Hidden Messages in the Replication Origin
/Problem 1 - Implement PatternCount


/q code
PatternCount:{sum x[1] ~/: sublist[;x[0]] each flip (til count x[0];count x[1])}

/testing with sample input
PatternCount[("ACTGTACGATGATGTGTGTCAAAG";"TGT")]

/testing with file input
filePath:`$":week1\\PatternCount\\dataset_2_6.txt"
PatternCount read0 filePath



/pseudocode
/
PatternCount(Text, Pattern)
  count ? 0
  for i ? 0 to |Text| ? |Pattern|
    if Text(i, |Pattern|) = Pattern
      count ? count + 1
  return count
\
