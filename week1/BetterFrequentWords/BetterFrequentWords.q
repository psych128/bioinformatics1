/BetterFrequentWords.q
/Chapter 1.2 Hidden Messages in the Replication Origin

/q code
freqMap:count each group sublist[;a] each flip (til count a;b)
(key freqMap) where (value freqMap) = max freqMap

/testing with sample input
a:"CGCCTAAATAGCCTCGCGGAGCCTTATGTCATACTCGTCCT"
b:3

a:"ACGTTGCATGTCGCATGATGCATGAGAGCT"
b:4


/testing with file input
filePath:`$":week1/BetterFrequentWords/dataset_2_13.txt"
data:read0 filePath
a:data[0]
b:value data[1]




/pseudocode
/
BetterFrequentWords(Text, k)
    FrequentPatterns ← an array of strings of length 0
    freqMap ← FrequencyTable(Text, k)
    max ← MaxMap(freqMap)
    for all strings Pattern in freqMap
        if freqMap[pattern] = max
            append Pattern to frequentPatterns
    return frequentPatterns
\