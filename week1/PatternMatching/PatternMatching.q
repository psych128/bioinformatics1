/PatternMatching.q
/Chapter 1.3 Some Hidden Messages are More Surprising than Others


/qcode
PatternMatch:{where x[0] ~/: sublist[;x[1]] each flip (til count x[1];count x[0])}


/file input
filePath:`$":week1/PatternMatching/dataset_3_5_2.txt"
data:read0 filePath
data:("ATAT";"GATATATGCATATACTT")
results:PatternMatch data
/for answer format
`int$results


/Vibrio_cholerae problem
/Return a space-separated list of starting positions (in increasing order) where CTTGATCAT appears as a substring in the Vibrio cholerae genome
filePath:`$":week1/PatternMatching/Vibrio_cholerae.txt"
genome:first read0 filePath
pattern:"CTTGATCAT"
pattern:"ATGATCAAG"
results:PatternMatch (pattern;genome)
`int$results
